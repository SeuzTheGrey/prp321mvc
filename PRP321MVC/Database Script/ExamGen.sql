USE [master]
GO
/****** Object:  Database [ExamGen]    Script Date: 2019/01/23 11:15:54 ******/
CREATE DATABASE [ExamGen]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ExamGen', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\ExamGen.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ExamGen_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\ExamGen_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ExamGen] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ExamGen].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ExamGen] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ExamGen] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ExamGen] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ExamGen] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ExamGen] SET ARITHABORT OFF 
GO
ALTER DATABASE [ExamGen] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [ExamGen] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [ExamGen] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ExamGen] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ExamGen] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ExamGen] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ExamGen] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ExamGen] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ExamGen] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ExamGen] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ExamGen] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ExamGen] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ExamGen] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ExamGen] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ExamGen] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ExamGen] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ExamGen] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ExamGen] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ExamGen] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ExamGen] SET  MULTI_USER 
GO
ALTER DATABASE [ExamGen] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ExamGen] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ExamGen] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ExamGen] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [ExamGen]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NULL,
	[AccPassword] [varchar](50) NULL,
	[AccPriority] [int] NULL,
	[isAdmin] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Answer]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Answer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[questionId] [int] NULL,
	[Answer] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Choices]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Choices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[QuestionID] [int] NULL,
	[Choice] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Course]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Course](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](30) NULL,
	[name] [varchar](30) NULL,
	[NQFLevel] [int] NULL,
	[CourseDescription] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CourseSubject]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CourseSubject](
	[CourseSubjectID] [int] IDENTITY(1,1) NOT NULL,
	[SubjectId] [int] NULL,
	[IdCourse] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CourseSubjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Exam]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Exam](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[LecturerID] [int] NULL,
	[ExamName] [varchar](50) NULL,
	[FilePath] [varchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lecturer]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lecturer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NULL,
	[name] [varchar](50) NULL,
	[surname] [varchar](50) NULL,
	[cell] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[LecturerLevel] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LecturerCourseSubject]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LecturerCourseSubject](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[LecturerID] [int] NULL,
	[CourseSubjectID] [int] NULL,
	[startDate] [date] NULL,
	[endDate] [date] NULL,
	[isFlagged] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Outcome]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Outcome](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SubjectID] [int] NULL,
	[name] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OutcomeDetails]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OutcomeDetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[OutcomeID] [int] NULL,
	[Material] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PracticalWeight]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PracticalWeight](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[QuestionId] [int] NULL,
	[QuestionBreakDown] [varchar](500) NULL,
	[practicalQuestionWeight] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Question]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Question](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[OutcomeDetailsID] [int] NULL,
	[QuestionTypeID] [int] NULL,
	[Questions] [varchar](500) NULL,
	[QuestionWeight] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuestionsType]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuestionsType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[QuestionType] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subjects]    Script Date: 2019/01/23 11:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subjects](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[code] [varchar](50) NULL,
	[duration] [varchar](50) NULL,
	[SubjectWeight] [int] NULL,
	[SubjectDescription] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD FOREIGN KEY([questionId])
REFERENCES [dbo].[Question] ([id])
GO
ALTER TABLE [dbo].[Choices]  WITH CHECK ADD FOREIGN KEY([QuestionID])
REFERENCES [dbo].[Question] ([id])
GO
ALTER TABLE [dbo].[CourseSubject]  WITH CHECK ADD FOREIGN KEY([IdCourse])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[CourseSubject]  WITH CHECK ADD FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subjects] ([id])
GO
ALTER TABLE [dbo].[Exam]  WITH CHECK ADD FOREIGN KEY([LecturerID])
REFERENCES [dbo].[Lecturer] ([id])
GO
ALTER TABLE [dbo].[Lecturer]  WITH CHECK ADD FOREIGN KEY([AccountID])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[LecturerCourseSubject]  WITH CHECK ADD FOREIGN KEY([CourseSubjectID])
REFERENCES [dbo].[CourseSubject] ([CourseSubjectID])
GO
ALTER TABLE [dbo].[LecturerCourseSubject]  WITH CHECK ADD FOREIGN KEY([LecturerID])
REFERENCES [dbo].[Lecturer] ([id])
GO
ALTER TABLE [dbo].[Outcome]  WITH CHECK ADD FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subjects] ([id])
GO
ALTER TABLE [dbo].[OutcomeDetails]  WITH CHECK ADD FOREIGN KEY([OutcomeID])
REFERENCES [dbo].[Outcome] ([id])
GO
ALTER TABLE [dbo].[PracticalWeight]  WITH CHECK ADD FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Question] ([id])
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD FOREIGN KEY([OutcomeDetailsID])
REFERENCES [dbo].[OutcomeDetails] ([id])
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD FOREIGN KEY([QuestionTypeID])
REFERENCES [dbo].[QuestionsType] ([id])
GO
USE [master]
GO
ALTER DATABASE [ExamGen] SET  READ_WRITE 
GO
