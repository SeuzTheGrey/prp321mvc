﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRP321MVC.Validation
{
    public class Validation
    {

        public bool checknull(string name, string surname, string phone, string email, string streetnum, string streetname, string suburb, string postalcode, string city, string province)
        {
            bool nonulls = false;
            if (name != "" && surname != "" && streetnum != "" && streetname != "" && suburb != "" && postalcode != "" && city != "" && province != "" && phone != "" && email != "")
            {
                nonulls = true;
            }
            return nonulls;
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public bool checkSame(string password, string confirm)
        {
            bool same = false;
            if (password == confirm)
            {
                same = true;
            }
            return same;
        }

        public bool checkPassword(string password)
        {

            const int MIN_LENGTH = 8;




            bool meetsLengthRequirements = password.Length >= MIN_LENGTH;
            bool hasUpperCaseLetter = false;
            bool hasLowerCaseLetter = false;
            bool hasDecimalDigit = false;

            if (meetsLengthRequirements)
            {
                foreach (char c in password)
                {
                    if (char.IsUpper(c)) hasUpperCaseLetter = true;
                    else if (char.IsLower(c)) hasLowerCaseLetter = true;
                    else if (char.IsDigit(c)) hasDecimalDigit = true;
                }
            }

            bool isValid = meetsLengthRequirements
                        && hasUpperCaseLetter
                        && hasLowerCaseLetter
                        && hasDecimalDigit
                        ;
            return isValid;

        }



    }



}